;;Spec symbol (ala tilde)
(require 'iso-transl)

(defun complete-url(url)
  "Checks if url is complete, if-not return complete one"
  (setq prefix "http://")
  (if (string-match prefix url)
      (setq ret  (concat url)))
  (if (not (string-match prefix url))
      (setq ret  (concat prefix url)))
  (message ret)
  )

(defun cfg-get-local-version()
  "Return the version number of the local custom cfg file (usually places in ~/.emacs.d/config.el)"
  (save-excursion
    (setq cfg-file "config.el")
    (find-file-noselect (concat user-emacs-directory cfg-file))
    (set-buffer cfg-file)
    (goto-char (point-min))
    (setq custom-init-version (string-to-number
			       (buffer-substring-no-properties
				(search-forward " ") (- (search-forward "\n") 1))))
    (kill-this-buffer)
    (print custom-init-version)
    )
  )

(defun cfg-get-current-version()
  "Returns the current config version"
  (string-to-number (url-get-source
		     "http://folk.uio.no/akhsarbg/emacs/.version"))
  )

;;Url
(defun url-get-source (url)
  "Returns ulr source code as string"
  (setq urlf (complete-url url))
  (let ((buffer (url-retrieve-synchronously urlf))
	(source nil))
    (save-excursion
      (set-buffer buffer)
      (goto-char (point-min))
      (re-search-forward "^$" nil 'move)
      (setq source (buffer-substring-no-properties (+ (point) 1) (- (point-max) 1)))
      (kill-buffer (current-buffer))
      source)
    )
  )


(defun cfg-pull-update()
  (setq body (url-get-source "http://folk.uio.no/akhsarbg/emacs/.config"))
  (setq cfg-file (concat user-emacs-directory "config.el"))
  (when (file-writable-p cfg-file)
    (with-temp-buffer
      (insert body)
      (write-region (point-min) (point-max) cfg-file nil)
      )
    )
  )


(defun check-cfg-update()
  "Return 1 if new version is available"
  (setq cfg-local (cfg-get-local--version))
  (setq cfg-curr  (cfg-get-current-version))
  (if (< cfg-local cfg-curr)
      (print 1))
  (if (not (< cfg-local cfg-curr))
      (print 0))
  )

;;Insert standart Java program code
(defun custom-insert-java-code()
  "Insert standart Java program code"
  (interactive)
  (insert (concat "public class " (substring (buffer-name) 0 -5) "{
    public static void main(String args[]){
        
    }
}"))
  (backward-char 8))
(global-set-key (kbd "C-x C-j") 'custom-insert-java-code)

;;Buffer/frame switching
(global-set-key (kbd "M-<up>") 'windmove-up)
(global-set-key (kbd "M-<down>") 'windmove-down)
(global-set-key (kbd "M-<left>") 'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)

;;Autosave before exit (No annoying questions)
(defun custon-autosave()
  (interactive)
  (save-buffer)
  (save-buffers-kill-terminal))
(global-set-key (kbd "C-x C-c") 'custon-autosave)
